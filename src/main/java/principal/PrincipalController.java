package principal;

import admission.GeneralInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class PrincipalController {

	@FXML
	private Button admission;
	
	public void admission(ActionEvent event) {
		new GeneralInfo().show();
	}	
}
