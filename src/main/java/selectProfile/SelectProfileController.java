package selectProfile;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import parent.WelcomeParent;
import student.WelcomeStudent;

public class SelectProfileController {
	@FXML
	private Button parent;
	
	@FXML
	private Button student;
	
	public void parent(ActionEvent event) {
		new WelcomeParent().show();
	}
	
	public void student(ActionEvent event) {
		new WelcomeStudent().show();
	}	
}
