package registration;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import tempData.RegistrationData;

public class RegistrationController {
	
	@FXML
	private TextField mobileNumber;
	@FXML
	private TextField grnNumber;
	@FXML
	private Button next;
	
	public void next(ActionEvent event) throws IOException {
	
		RegistrationData t = new RegistrationData();
		t.setGrnNumber(grnNumber.getText());
		t.setMobileNumber(mobileNumber.getText());
		new RegistrationPassword().show();
				
	}
		
}	

