package login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import principal.Principal;
import selectProfile.SelectProfile;
import student.WelcomeStudent;

public class LoginController {

	@FXML
	private TextField mobileNumber;

	@FXML
	private TextField passcode;

	@FXML
	private Button next;
	
	public void next(ActionEvent event)throws IOException{
		// The message that is going to be sent to the server
				// using the POST request
		final String messageContent = "{\n" + "\"mobileNumber\"" + ":\"" + mobileNumber.getText() + "\", \r\n"
				+ "\"passcode\"" + ":\"" + passcode.getText() + "\" \r\n" +"\n}";
				
				// Printing the message
				System.out.println(messageContent);

	 			// URL of the API or Server
				String url = "http://localhost:8080/user/api/v1/login";
				URL urlObj = new URL(url);
				HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
				postCon.setRequestMethod("POST");
				postCon.setRequestProperty("userId", "abcdef");
				
				// Setting the message content type as JSON
				postCon.setRequestProperty("Content-Type", "application/json");
				postCon.setDoOutput(true);
				
				// for writing the message content to the server
				OutputStream osObj = postCon.getOutputStream();
				osObj.write(messageContent.getBytes());
				
				// closing the output stream
				osObj.flush();
				osObj.close();
				int respCode = postCon.getResponseCode();
				System.out.println("Response from the server is: \n");
				System.out.println("The POST Request Response Code :  " + respCode);
				System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
				if (respCode == HttpURLConnection.HTTP_CREATED) {
					// reaching here means the connection has been established
					// By default, InputStream is attached to a keyboard.
					// Therefore, we have to direct the InputStream explicitly
					// towards the response of the server
					InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
					BufferedReader br = new BufferedReader(irObj);
					String input = null;
					StringBuffer sb = new StringBuffer();
					while ((input = br.readLine()) != null) {
						sb.append(input);
					}
					br.close();
					postCon.disconnect();
					// printing the response
					
					System.out.println(sb.toString());
					
					if (sb.toString().contains("Parent")) {
						System.out.println("Welcome Parent");
						new SelectProfile().show();
					}

					if (sb.toString().contains("Student")) {
						System.out.println("Welcome Student");
						new WelcomeStudent().show();
					}
					
					if (sb.toString().contains("principal")) {
						System.out.println("Welcome principal");
						new Principal().show();
					}

				} else {

					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("Invalid Mobile Number or Password");
					alert.showAndWait();
				}
			}
		}
