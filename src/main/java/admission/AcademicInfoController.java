package admission;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import tempData.AdmissionData;

public class AcademicInfoController {

	@FXML
	private TextField studentBirthDate;
	
	@FXML
	private ComboBox gender;
	
	@FXML
	private ComboBox applyClass;
	
	@FXML
	private TextField previousSchoolName;

	@FXML
	private TextField studentMobileNumber;

	@FXML
	private TextField email;
	
	@FXML
	private Button save;
	
	
	AdmissionData t= new AdmissionData();
	private String studentFirstName = t.admissionData[0];
	private String fatherFirstName = t.admissionData[1];
	private String motherFirstName = t.admissionData[2];
	private String lastName = t.admissionData[3];
	private String parentMobileNumber = t.admissionData[4];
	private String address = t.admissionData[5];
	
	public void save(ActionEvent event) throws IOException {
		
		
		// The message that is going to be sent to the server
		// using the POST request
		

		final String messageContent = "{\n" + 
										"\"studentFirstName\"" + ":\"" + studentFirstName + "\", \r\n" +
										"\"fatherFirstName\"" + ":\"" + fatherFirstName + "\", \r\n" +
										"\"motherFirstName\"" + ":\"" + motherFirstName + "\", \r\n" +
										"\"lastName\"" + ":\"" + lastName + "\", \r\n" +
										"\"parentMobileNumber\"" + ":\"" + parentMobileNumber + "\", \r\n" +
										"\"address\"" + ":\"" + address + "\", \r\n" +
										"\"studentBirthDate\"" + ":\"" + studentBirthDate.getText() + "\", \r\n" +
										"\"gender\"" + ":\"" + gender.getValue() + "\", \r\n" +
										"\"applyClass\"" + ":\"" + applyClass.getValue() + "\", \r\n" +
										"\"previousSchoolName\"" + ":\"" + previousSchoolName.getText() + "\", \r\n" +
										"\"studentMobileNumber\"" + ":\"" + studentMobileNumber.getText() + "\", \r\n" +
										"\"email\"" + ":\"" + email.getText() + "\" \r\n" +
										"\n}";

		// Printing the message
		System.out.println(messageContent);

		// URL of the API or Server
		String url = "http://localhost:8080/student/api/v1/admission";
		URL urlObj = new URL(url);
		HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
		postCon.setRequestMethod("POST");
		postCon.setRequestProperty("userId", "abcdef");

		// Setting the message content type as JSON
		postCon.setRequestProperty("Content-Type", "application/json");
		postCon.setDoOutput(true);

		// for writing the message content to the server
		OutputStream osObj = postCon.getOutputStream();
		osObj.write(messageContent.getBytes());

		// closing the output stream
		osObj.flush();
		osObj.close();
		int respCode = postCon.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {
			// reaching here means the connection has been established
		    // By default, InputStream is attached to a keyboard.
			// Therefore, we have to direct the InputStream explicitly
	        // towards the response of the server
			InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			postCon.disconnect();
			// printing the response
			System.out.println(sb.toString());
	       // new HomeScreen2().show();

		} else {
			// connection was not successful
			System.out.println("POST Request did not work.");
		}
	}
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list = FXCollections.observableArrayList("Male", "Female");
		gender.setItems(list);
		//userRole.getItems().add(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Nursary", "Kinder Garden");
		applyClass.setItems(list2);
	}
}
