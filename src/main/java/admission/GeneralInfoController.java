package admission;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import tempData.AdmissionData;

public class GeneralInfoController {
	
	@FXML
	private TextField studentFirstName;
	@FXML
	private TextField fatherFirstName;
	@FXML
	private TextField motherFirstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField parentMobileNumber;
	@FXML
	private TextField address;
	@FXML
	private Button next;
	
	public void next(ActionEvent event) throws IOException {
		
		AdmissionData t = new AdmissionData();
		t.setStudentFirstName(studentFirstName.getText());
		t.setFatherFirstName(fatherFirstName.getText());
		t.setMotherFirstName(motherFirstName.getText());
		t.setLastName(lastName.getText());
		t.setParentMobileNumber(parentMobileNumber.getText());
		t.setAddress(address.getText());
		
		new AcademicInfo().show();
				
	}
}
