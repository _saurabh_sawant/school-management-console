package welcome;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import registration.Registration;

public class WelcomeController {
	@FXML
	private Button login;
	
	@FXML
	private Button registration;
	
	public void login(ActionEvent event) {
		new Login().show();
	}
	
	public void registration(ActionEvent event) {
		new Registration().show();
	}	
}
